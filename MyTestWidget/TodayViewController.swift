//
//  TodayViewController.swift
//  MyTestWidget
//
//  Created by Joseph on 2019/3/19.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit
import NotificationCenter
//import os

class TodayViewController: UIViewController, NCWidgetProviding {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        //os_log("TodayViewController viewDidLoad")
        print("TodayViewController viewDidLoad")
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("TodayViewController viewWillAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("TodayViewController viewWillDisappear")
        super.viewWillDisappear(animated)
        
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        //os_log("TodayViewController widgetPerformUpdate")
        print("TodayViewController widgetPerformUpdate")
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded {
            self.preferredContentSize = CGSize(width: maxSize.width, height: 500)
        }else {
            self.preferredContentSize = maxSize
        }
    }
    
    @IBAction func btnDidTap(_ sender: Any) {
        let url = URL(string: "myWidget://")!
        self.extensionContext?.open(url, completionHandler: nil)
    }
}
