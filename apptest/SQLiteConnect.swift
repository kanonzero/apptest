//
//  SQLiteConnect.swift
//
//  Created by Joseph on 2019/2/18.
//  Copyright © 2019 Joseph. All rights reserved.
//
import UIKit

class SQLiteConnect {
    
    var db :OpaquePointer? = nil
    let sqlitePath :String
    
    init?() {
        let urls = FileManager.default
            .urls(
                for: .documentDirectory,
                in: .userDomainMask)
        sqlitePath = urls[urls.count-1].absoluteString
            + "names.sqlite"
        
        if(!self.openDatabase(path: sqlitePath)){
            print("!= Unable to open database.")
            return nil
        }else{

            print("Successfully opened database.")
            if db == nil {
                print("db == nil")
                return nil
            }
        }
    }
    
    /**
       開啟database
     
       - Parameter path: database路徑
       - Returns: 是否開啟成功
    */
    func openDatabase(path :String) -> Bool {
        if sqlite3_open_v2(path, &db, SQLITE_OPEN_CREATE|SQLITE_OPEN_READWRITE|SQLITE_OPEN_FULLMUTEX, nil) == SQLITE_OK {

            print("Successfully opened database \(path)")
            return true
        } else {

            print("Unable to open database.")
            return false
        }
    }
    
    /**
     close DB
     
     */
    func closeDB()
    {
        if db != nil {
            sqlite3_close(db)
            db = nil
        }
    }
    
    /**
       建立資料表 create table
     
    */
    func createTable(){
        
        //所在區域、停車場名稱、地址、開放時間
        let sql1 = "create table if not exists parking_list "
            + "( id integer primary key autoincrement, "
            + "name char, area char, address char, service_time char);" as NSString
        
        let sql = "\(sql1);" as NSString
        print("sql command\(sql)")
        if sqlite3_exec(db, sql.utf8String, nil, nil, nil)
            == SQLITE_OK{
            print("creating table successfully")
        }
        
    }
    
    /**
     寫入資料
     
     - Parameter pushData: server上的資料
     
     */
    func insertParkingDataAll(_ pushData:[String:AnyObject]){
        let name = pushData["NAME"] as! String
        let area = pushData["AREA"] as! String
        let address = pushData["ADDRESS"] as! String
        let service_time = pushData["SERVICETIME"] as! String
        
//        print("name: \(name)")
//        print("area: \(area)")
//        print("address: \(address)")
//        print("service_time: \(service_time)")

        var statement :OpaquePointer? = nil
        let sqlinsert = "insert into parking_list "
            + "(name, area, address, service_time) "
            + "values ('\(name)', '\(area)', '\(address)', '\(service_time)')" as NSString
        
        
//        print("select command \(sqlinsert)")
        
        if sqlite3_prepare_v2(db, sqlinsert.utf8String, -1, &statement, nil) == SQLITE_OK
        {
            if sqlite3_step(statement) == SQLITE_DONE {
//                print("Insert data successfully")
            }
            
            sqlite3_finalize(statement)
        }
        else{
            print("Insert data unsuccessfully")
            print("but failed:\(String(cString:sqlite3_errmsg(db)))")
        }
    }
    
    /**
     清空資料
     
     */
    func deleteParkingData(){
        var statement :OpaquePointer? = nil
        var sqlinsert:NSString = ""
        
        sqlinsert = "delete from parking_list" as NSString
        
        
//        print("select command \(sqlinsert)")
        
        if sqlite3_prepare_v2(db, sqlinsert.utf8String, -1, &statement, nil) == SQLITE_OK
        {
            print("deleteParkingData SQLITE_OK")
            if sqlite3_step(statement) == SQLITE_DONE {
//                print("deleteParkingData successfully")
            }
                
            else
            {
                let errmsg = String(cString:sqlite3_errmsg(db))
                print("sql fail:",errmsg)
            }
            
            // 5.重置STMT
            if sqlite3_reset(statement) != SQLITE_OK
            {
                print("reset fail")
            }
        }
        else{
            print("deleteParkingData unsuccessfully")
        }
        sqlite3_finalize(statement)
        
    }
    
    /**
     取得所有資料
     
     - Returns: 所有資料
     
     */
    func selectParkingData() -> Array<[String:AnyObject]>{
        var statement :OpaquePointer? = nil
        var returnedArray:Array<[String:AnyObject]> = Array()
        //let sqlselect = "select * from names"
        let sqlselect = "SELECT * FROM parking_list"
        
        if sqlite3_prepare_v2(db, (sqlselect as NSString).utf8String, -1, &statement, nil) == SQLITE_OK{
        }
            
            
        else {
            
            let errmsg = String(cString:sqlite3_errmsg(db))
            print("sql fail:\(errmsg)")
        }
        
        while sqlite3_step(statement) == SQLITE_ROW{
            let dataDic:[String:AnyObject] = ["name":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 1))))" as AnyObject,
                                              "area":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 2))))" as AnyObject,
                                              "address":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 3))))" as AnyObject,
                                              "service_time":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 4))))" as AnyObject]
            
            returnedArray.append(dataDic)
            
            
        }
        
        sqlite3_finalize(statement)
        
        return returnedArray
    }
    
    /**
     取得含有關鍵字的資料
     
     - Returns: 含有關鍵字的資料
     
     */
    func selectParkingData(keyword:String) -> Array<[String:AnyObject]>{
        var statement :OpaquePointer? = nil
        var returnedArray:Array<[String:AnyObject]> = Array()
        let sqlselect = "SELECT * FROM parking_list WHERE name LIKE '%\(keyword)%' OR area LIKE '%\(keyword)%'"
        print("selectParkingData sqlselect:\(sqlselect)")
        if sqlite3_prepare_v2(db, (sqlselect as NSString).utf8String, -1, &statement, nil) == SQLITE_OK{
        }
            
            
        else {
            
            let errmsg = String(cString:sqlite3_errmsg(db))
            print("sql fail:\(errmsg)")
        }
        
        while sqlite3_step(statement) == SQLITE_ROW{
            let dataDic:[String:AnyObject] = ["name":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 1))))" as AnyObject,
                                              "area":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 2))))" as AnyObject,
                                              "address":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 3))))" as AnyObject,
                                              "service_time":"\(String(cString: UnsafePointer(sqlite3_column_text(statement, 4))))" as AnyObject]
            
            returnedArray.append(dataDic)
            
            
        }
        print("returnedArray:\(returnedArray)")
        
        sqlite3_finalize(statement)
        
        return returnedArray
    }
    
}

