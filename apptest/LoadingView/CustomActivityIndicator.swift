//
//  CustomActivityIndicator.swift
//
//  Created by Joseph on 2019/2/18.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit

class CustomActivityIndicator: UIView {
    
    var activityIndicatorContainer: UIView = UIView()
    var mActivityIndicator:UIImageView = UIImageView();
    
    let fullScreenSize = UIScreen.main.bounds.size

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        activityIndicatorContainer.frame = CGRect(x:0, y:0, width:fullScreenSize.width, height:fullScreenSize.height)
        
        if(fullScreenSize.width>414){
            mActivityIndicator.frame = CGRect(x:fullScreenSize.width/2-90, y:fullScreenSize.height/2-90, width:180.0, height:180.0)
        }else{
            mActivityIndicator.frame = CGRect(x:fullScreenSize.width/2-50, y:fullScreenSize.height/2-50, width:100.0, height:100.0)
        }
        
        //activityIndicatorContainer.layer.cornerRadius = 10
        //activityIndicatorContainer.layer.borderWidth = 0.5
        //activityIndicatorContainer.layer.borderColor = UIColor.gray.cgColor
        activityIndicatorContainer.backgroundColor = UIColor(
            red:0/255,
            green:0/255,
            blue:0/255,
            alpha:0.3)
        
        mActivityIndicator.image = UIImage(named: "ic_loading.png")
        
        activityIndicatorContainer.addSubview(mActivityIndicator)
        self.addSubview(activityIndicatorContainer)
        
        //self.addSubview(mActivityIndicator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getCustomActivityIndicator() -> UIImageView{
        return mActivityIndicator
    }

}
