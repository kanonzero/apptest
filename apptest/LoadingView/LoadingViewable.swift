//
//  LoadingViewable.swift
//  MVVMRx
//
//  Created by Mohammad Zakizadeh on 7/26/18.
//  Copyright © 2018 Storm. All rights reserved.
//

import UIKit


protocol loadingViewable {
    func startActivityIndicator()
    func finishActivityIndicator()
}
extension loadingViewable where Self : UIViewController {
    func startActivityIndicator(){
        let mCustomActivityIndicator = CustomActivityIndicator(frame: CGRect.zero)
        view.addSubview(mCustomActivityIndicator)
        view.bringSubviewToFront(mCustomActivityIndicator)
        mCustomActivityIndicator.restorationIdentifier = "loadingView"
        mCustomActivityIndicator.getCustomActivityIndicator().startRotate()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    func finishActivityIndicator() {
        for item in view.subviews
            where item.restorationIdentifier == "loadingView" {
                (item as! CustomActivityIndicator).getCustomActivityIndicator().stopRotate()
                item.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
}
