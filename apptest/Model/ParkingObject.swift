//
//  ParkingObject.swift
//  apptest
//
//  Created by Joseph on 2019/3/14.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Parking: Object, Decodable{
    @objc dynamic var id: String = ""
    @objc dynamic var area: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var servicetime: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case area = "AREA"
        case name = "NAME"
        case address = "ADDRESS"
        case servicetime = "SERVICETIME"
    }
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        area = try container.decode(String.self, forKey: .area)
        name = try container.decode(String.self, forKey: .name)
        address = try container.decode(String.self, forKey: .address)
        servicetime = try container.decode(String.self, forKey: .servicetime)
        
        super.init()
    }
    
    required init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init(realm: realm, schema: schema)
    }
    
}

class ParkingResult: Decodable{
    
    var records:[Parking]
    
    enum CodingKeys: String, CodingKey {
        case records = "records"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        records = try container.decode([Parking].self, forKey: .records)
        
    }
}

class ParkingRequest: Decodable{
    //{"success":true,"result":{"resource_id":"382000000A-000225-002","limit":2000,"total":933,"fields":[{"type":"text","id":"ID"},{"type":"text","id":"AREA"},{"type":"text","id":"NAME"},{"type":"text","id":"TYPE"},{"type":"text","id":"SUMMARY"},{"type":"text","id":"ADDRESS"},{"type":"text","id":"TEL"},{"type":"text","id":"PAYEX"},{"type":"text","id":"SERVICETIME"},{"type":"text","id":"TW97X"},{"type":"text","id":"TW97Y"},{"type":"text","id":"TOTALCAR"},{"type":"text","id":"TOTALMOTOR"},{"type":"text","id":"TOTALBIKE"}]
    var success:Bool
    var result:ParkingResult
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case result = "result"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        success = try container.decode(Bool.self, forKey: .success)
        result = try container.decode(ParkingResult.self, forKey: .result)
        
    }
    
}
