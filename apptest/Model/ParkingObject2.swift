//
//  ParkingObject.swift
//  apptest
//
//  Created by Joseph on 2019/3/14.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    func managedObject() -> ManagedObject
}

final class StoredParking: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var area: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var servicetime: String = ""
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
}
