//
//  RealmObject.swift
//  apptest
//
//  Created by Joseph on 2019/3/14.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public final class WriteTransaction {
    private let realm: Realm
    internal init(realm: Realm) {
        self.realm = realm
    }
    public func beginWrite(){
        realm.beginWrite()
    }
    public func commitWrite(){
        print("fileURL: \(realm.configuration.fileURL!)")
        try! realm.commitWrite()
    }
    public func add<T: Persistable>(_ value: T, update: Bool) {
        realm.add(value.managedObject(), update: update)
    }
}
// Implement the Container
public final class Container {
    private let realm: Realm
    public convenience init() throws {
        try self.init(realm: Realm())
    }
    internal init(realm: Realm) {
        self.realm = realm
    }
    /*public func write(_ block: (WriteTransaction) throws -> Void)
     throws {
     let transaction = WriteTransaction(realm: realm)
     try realm.write {
     try block(transaction)
     }
     }*/
    public func write(_ block: (WriteTransaction) throws -> Void)
        throws {
            let transaction = WriteTransaction(realm: realm)
            try block(transaction)
    }
}

