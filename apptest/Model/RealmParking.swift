//
//  RealmParking.swift
//  apptest
//
//  Created by Joseph on 2019/3/14.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

@objcMembers class StoredParkingRequest: Object, Decodable {
    dynamic var success:Bool
    let results = RealmSwift.List<StoredParkingResult>()
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case results = "result"
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        success = try container.decode(Bool.self, forKey: .success)
        
        let result = try container.decode(StoredParkingResult.self, forKey: .results)
        results.append(result)
        
        super.init()
    }
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    required init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init(realm: realm, schema: schema)
    }
}

@objcMembers class StoredParkingResult: Object, Decodable {
    let records = RealmSwift.List<StoredParking>()
    
    enum CodingKeys: String, CodingKey {
        case records = "records"
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let record = try container.decode([StoredParking].self, forKey: .records)
        
        records.append(objectsIn: record)
        
        super.init()
    }
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    required init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init(realm: realm, schema: schema)
    }
}

@objcMembers class StoredParking: Object, Decodable {
    dynamic var id: String = ""
    dynamic var area: String = ""
    dynamic var name: String = ""
    dynamic var address: String = ""
    dynamic var servicetime: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case area = "AREA"
        case name = "NAME"
        case address = "ADDRESS"
        case servicetime = "SERVICETIME"
    }
    
    required  init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        area = try container.decode(String.self, forKey: .area)
        name = try container.decode(String.self, forKey: .name)
        address = try container.decode(String.self, forKey: .address)
        servicetime = try container.decode(String.self, forKey: .servicetime)
        
        super.init()
    }
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    required init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init(realm: realm, schema: schema)
    }
}
