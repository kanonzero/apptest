//
//  DouBanAPI.swift
//  hangge_1797
//
//  Created by hangge on 2017/9/19.
//  Copyright © 2017年 hangge.com. All rights reserved.
//

import Foundation
import Moya
import Alamofire

/*let manager = Manager(
    configuration: URLSessionConfiguration.default,
    serverTrustPolicyManager: CustomServerTrustPoliceManager()
)


let UniversalProvider = MoyaProvider<RequestAPI>(manager: manager,
                                       plugins: [NetworkLoggerPlugin(verbose: true)])*/
//let UniversalProvider = MoyaProvider<Universal>()

//请求分类
public enum RequestAPI {
    case getParkingList()
}

//请求配置
extension RequestAPI: TargetType {
    
    //服务器地址
    public var baseURL: URL {
        let path = "https://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000225-002"
        return URL(string: path)!
    }
    
    //各个请求的具体路径
    public var path: String {
        return ""
    }
    
    //请求类型
    public var method: Moya.Method {
        return .get
    }
    
    //请求任务事件（这里附带上参数）
    public var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }
    
    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }
    
    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
    
    //请求头
    public var headers: [String: String]? {
        return nil
    }
}

//定义下载的DownloadDestination（不改变文件名，同名文件不会覆盖）
/*private let DefaultDownloadDestination: DownloadDestination = { temporaryURL, response in
    return (DefaultDownloadDir.appendingPathComponent(response.suggestedFilename!),
            [.removePreviousFile])
}*/

//默认下载保存地址（用户文档目录）
let DefaultDownloadDir: URL = {
    let directoryURLs = FileManager.default.urls(for: .documentDirectory,
                                                 in: .userDomainMask)
    return directoryURLs.first ?? URL(fileURLWithPath: NSTemporaryDirectory())
}()


/*class CustomServerTrustPoliceManager : ServerTrustPolicyManager {
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        return .disableEvaluation
    }
    public init() {
        super.init(policies: [:])
    }
}*/
