//
//  MoyaHttpmanager.swift
//  meshrouter
//
//  Created by Joseph on 2019/2/26.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import Moya
import Alamofire
import RxSwift
import RxCocoa

/// 超时时长
private var requestTimeOut:Double = 30
///成功数据的回调
typealias successCallback = ((Data) -> (Void))
typealias donwloadSuccessCallback = ((Int) -> (Void))
typealias uploadSuccessCallback = ((Int) -> (Void))
typealias transferSuccessCallback = ((Int) -> (Void))
///失败的回调
typealias failedCallback = ((String) -> (Void))
///网络错误的回调
typealias errorCallback = (() -> (Void))

let disposeBag = DisposeBag()

///网络请求的基本设置,这里可以拿到是具体的哪个网络请求，可以在这里做一些设置
private let myEndpointClosure = { (target: RequestAPI) -> Endpoint in
    ///这里把endpoint重新构造一遍主要为了解决网络请求地址里面含有? 时无法解析的bug https://github.com/Moya/Moya/issues/1198
    let url = target.baseURL.absoluteString + target.path
    var task = target.task
    
    /*
     如果需要在每个请求中都添加类似token参数的参数请取消注释下面代码
     */
    //    let additionalParameters = ["token":"888888"]
    //    let defaultEncoding = URLEncoding.default
    //    switch target.task {
    //        ///在你需要添加的请求方式中做修改就行，不用的case 可以删掉。。
    //    case .requestPlain:
    //        task = .requestParameters(parameters: additionalParameters, encoding: defaultEncoding)
    //    case .requestParameters(var parameters, let encoding):
    //        additionalParameters.forEach { parameters[$0.key] = $0.value }
    //        task = .requestParameters(parameters: parameters, encoding: encoding)
    //    default:
    //        break
    //    }
    /*
     如果需要在每个请求中都添加类似token参数的参数请取消注释上面代码
     */
    
    
    var endpoint = Endpoint(
        url: url,
        sampleResponseClosure: { .networkResponse(200, target.sampleData) },
        method: target.method,
        task: task,
        httpHeaderFields: target.headers
    )
    requestTimeOut = 30//每次请求都会调用endpointClosure 到这里设置超时时长 也可单独每个接口设置
    /*switch target {
    case .easyRequset:
        return endpoint
    case .register:
        requestTimeOut = 5
        return endpoint
        
    default:
        return endpoint
    }*/
    return endpoint
}

///网络请求的设置
private let requestClosure = { (endpoint: Endpoint, done: MoyaProvider.RequestResultClosure) in
    do {
        var request = try endpoint.urlRequest()
        //设置请求时长
        request.timeoutInterval = requestTimeOut
        // 打印请求参数
        if let requestData = request.httpBody {
            print("\(request.url!)"+"\n"+"\(request.httpMethod ?? "")"+"发送参数"+"\(String(data: request.httpBody!, encoding: String.Encoding.utf8) ?? "")")
        }else{
            print("\(request.url!)"+"\(String(describing: request.httpMethod))")
        }
        done(.success(request))
    } catch {
        done(.failure(MoyaError.underlying(error, nil)))
    }
}

/*   设置ssl
 let policies: [String: ServerTrustPolicy] = [
 "example.com": .pinPublicKeys(
 publicKeys: ServerTrustPolicy.publicKeysInBundle(),
 validateCertificateChain: true,
 validateHost: true
 )
 ]
 */

// 用Moya默认的Manager还是Alamofire的Manager看实际需求。HTTPS就要手动实现Manager了
//private public func defaultAlamofireManager() -> Manager {
//
//    let configuration = URLSessionConfiguration.default
//
//    configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
//
//    let policies: [String: ServerTrustPolicy] = [
//        "ap.grtstar.cn": .disableEvaluation
//    ]
//    let manager = Alamofire.SessionManager(configuration: configuration,serverTrustPolicyManager: ServerTrustPolicyManager(policies: policies))
//
//    manager.startRequestsImmediately = false
//
//    return manager
//}

/*let pathToCert = Bundle.main.path(forResource: "smart_air_center", ofType: "cer")
let localCertificate = NSData(contentsOfFile: pathToCert!)
let certificates = [SecCertificateCreateWithData(nil, localCertificate!)!]

let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
    certificates: certificates,
    validateCertificateChain: true,
    validateHost: true
)
var serverTrustPolicies = ["52.2.95.137" : serverTrustPolicy]
var serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)

var manager = Manager(
    configuration: URLSessionConfiguration.default,
    serverTrustPolicyManager: serverTrustPolicyManager
)*/

let manager = Manager(
 configuration: URLSessionConfiguration.default,
 serverTrustPolicyManager: CustomServerTrustPoliceManager()
 )


/// NetworkActivityPlugin插件用来监听网络请求，界面上做相应的展示
///但这里我没怎么用这个。。。 loading的逻辑直接放在网络处理里面了
private let networkPlugin = NetworkActivityPlugin.init { (changeType, targetType) in
    
    print("networkPlugin \(changeType)")
    //targetType 是当前请求的基本信息
    switch(changeType){
    case .began:
        print("开始请求网络")
        
    case .ended:
        print("结束")
    }
}

// https://github.com/Moya/Moya/blob/master/docs/Providers.md  参数使用说明
//stubClosure   用来延时发送网络请求


////网络请求发送的核心初始化方法，创建网络请求对象
let Provider:MoyaProvider<RequestAPI> = MoyaProvider<RequestAPI>( endpointClosure: myEndpointClosure, requestClosure: requestClosure, manager: manager, plugins: [networkPlugin], trackInflights: false)

/*private func setNetworkManager(ip:String){
    serverTrustPolicies = [ip : serverTrustPolicy]
    serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
    
    manager = Manager(
        configuration: URLSessionConfiguration.default,
        serverTrustPolicyManager: serverTrustPolicyManager
    )
}*/


/// 最常用的网络请求，只需知道正确的结果无需其他操作时候用这个
///
/// - Parameters:
///   - target: 网络请求
///   - completion: 请求成功的回调
func NetWorkRequest(_ target: RequestAPI, completion: @escaping successCallback ){
    NetWorkRequest(target, completion: completion, failed: nil, errorResult: nil)
}


/// 需要知道成功或者失败的网络请求， 要知道code码为其他情况时候用这个
///
/// - Parameters:
///   - target: 网络请求
///   - completion: 成功的回调
///   - failed: 请求失败的回调
func NetWorkRequest(_ target: RequestAPI, completion: @escaping successCallback , failed:failedCallback?) {
    NetWorkRequest(target, completion: completion, failed: failed, errorResult: nil)
}


///  需要知道成功、失败、错误情况回调的网络请求   像结束下拉刷新各种情况都要判断
///
/// - Parameters:
///   - target: 网络请求
///   - completion: 成功
///   - failed: 失败
///   - error: 错误
func NetWorkRequest(_ target: RequestAPI, completion: @escaping successCallback , failed:failedCallback?, errorResult:errorCallback?) {
    //先判断网络是否有链接 没有的话直接返回--代码略
    if !isNetworkConnect{
        print("提示用户网络似乎出现了问题")
        if errorResult != nil {
            errorResult!()
            return
        }
    }
    
    Provider.rx.request(target)
        .subscribe(onSuccess: { response in
            completion(response.data)
    },onError: { error in
        print("数据请求失败!错误原因：", error)
        if errorResult != nil {
            errorResult!()
        }
    }).disposed(by: disposeBag)
    /*Provider.rx.request(target)
        .mapJSON()
        .subscribe(onSuccess: { json in
            let dic:[String:AnyObject] = json as! [String: AnyObject]
            print("vc 得到資料 dic:  \(dic)")
            completion(dic)
        },onError: { error in
            print("数据请求失败!错误原因：", error)
            if errorResult != nil {
                errorResult!()
            }
        }).disposed(by: disposeBag)*/
    
    
    /*Provider.request(target) { (result) in
        //隐藏hud
        switch result {
        case let .success(response):
            do {
                let jsonData = try JSON(data: response.data)
                print("NetWorkRequest jsonData:\(jsonData)")
                let dic:[String:AnyObject] = try JSONSerialization.jsonObject(with: response.data, options: []) as! [String:AnyObject]
                if(Config.debug == true){
                    log.debug("vc 得到資料 dic:  \(dic)")
                }
                completion(dic)
                
            } catch {
            }
        case let .failure(error):
            guard let error = error as? CustomStringConvertible else {
                //网络连接失败，提示用户
                print("网络连接失败")
                break
            }
            /*if failed != nil {
                failed!("-404")
                return
            }*/
            if errorResult != nil {
                errorResult!()
            }
        }
    }*/
}

func NetWorkTransferFileRequest(_ target: RequestAPI, completion: @escaping transferSuccessCallback , failed:failedCallback?, errorResult:errorCallback?) {
    //先判断网络是否有链接 没有的话直接返回--代码略
    if !isNetworkConnect{
        print("提示用户网络似乎出现了问题")
        if errorResult != nil {
            errorResult!()
            return
        }
    }
    Provider.rx.requestWithProgress(target).subscribe { event in
        switch event {
        case .next(let progressResponse):
            if let response = progressResponse.response {
                // do something with response
                print("success response:\(response.statusCode)")
                completion(response.statusCode)
            } else {
                print("Progress: \(progressResponse.progress)")
            }
        case .error(let error):
        // handle the error
            guard (error as? CustomStringConvertible) != nil else {
                //网络连接失败，提示用户
                print("网络连接失败")
                break
            }
            /*if failed != nil {
             failed!("-404")
             return
             }*/
            if errorResult != nil {
                errorResult!()
            }
        default:
            break
        }
    }.disposed(by: disposeBag)
    /*Provider.request(target, callbackQueue: DispatchQueue.main, progress: { (response) in
        
        print("download progress:\(response.progress)")
        
    }) { (result) in
        switch result {
        case let .success(response):
            print("success response:\(response.statusCode)")
            completion(response.statusCode)
        case let .failure(error):
            guard let error = error as? CustomStringConvertible else {
                //网络连接失败，提示用户
                print("网络连接失败")
                break
            }
            /*if failed != nil {
             failed!("-404")
             return
             }*/
            if errorResult != nil {
                errorResult!()
            }
        }
    }*/
}

/*func NetWorkUploadRequest(_ target: RequestAPI, completion: @escaping uploadSuccessCallback , failed:failedCallback?, errorResult:errorCallback?) {
    //先判断网络是否有链接 没有的话直接返回--代码略
    if !isNetworkConnect{
        print("提示用户网络似乎出现了问题")
        if errorResult != nil {
            errorResult!()
            return
        }
    }
    Provider.request(target, callbackQueue: DispatchQueue.main, progress: { (response) in
        
        print("download progress:\(response.progress)")
        
    }) { (result) in
        switch result {
        case let .success(response):
            print("success response:\(response.statusCode)")
            completion(response.statusCode)
        case let .failure(error):
            guard let error = error as? CustomStringConvertible else {
                //网络连接失败，提示用户
                print("网络连接失败")
                break
            }
            /*if failed != nil {
             failed!("-404")
             return
             }*/
            if errorResult != nil {
                errorResult!()
            }
        }
    }
}*/


/// 基于Alamofire,网络是否连接，，这个方法不建议放到这个类中,可以放在全局的工具类中判断网络链接情况
/// 用get方法是因为这样才会在获取isNetworkConnect时实时判断网络链接请求，如有更好的方法可以fork
var isNetworkConnect: Bool {
    get{
        let network = NetworkReachabilityManager()
        return network?.isReachable ?? true //无返回就默认网络已连接
    }
}

/// Demo中并未使用，以后如果有数组转json可以用这个。
struct JSONArrayEncoding: ParameterEncoding {
    static let `default` = JSONArrayEncoding()
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        
        guard let json = parameters?["jsonArray"] else {
            return request
        }
        
        let data = try JSONSerialization.data(withJSONObject: json, options: [])
        
        if request.value(forHTTPHeaderField: "Content-Type") == nil {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        request.httpBody = data
        
        return request
    }
}

class CustomServerTrustPoliceManager : ServerTrustPolicyManager {
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        return .disableEvaluation
    }
    public init() {
        super.init(policies: [:])
    }
}
