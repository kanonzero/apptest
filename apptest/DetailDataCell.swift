//
//  DetailDataCell.swift
//  apptest
//
//  Created by Joseph on 2019/2/19.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit

class DetailDataCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var service_time: UILabel!
    @IBOutlet weak var address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
