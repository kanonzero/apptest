//
//  CustomBackBtn.swift
//
//  Created by Joseph on 2019/2/18.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit

class CustomBackBtn: UIButton {
    
    var insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var rect = bounds
        rect.origin.x -= insets.left
        rect.origin.y -= insets.top
        rect.size.width += insets.left + insets.right
        rect.size.height += insets.top + insets.bottom
        
        return rect.contains(point)
    }
}
