//
//  HttpManager.swift
//
//  Created by Joseph on 2019/2/18.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation

protocol HttpManagerDelegate{
    func httpResponseResult(commd:String, returnDic:[String:AnyObject])
    func httpRequestError(commd:String)
}

class HttpManager:NSObject, URLSessionDelegate{
    
    var delegate:HttpManagerDelegate?
    var httpSession: URLSession!
    var httpTask: URLSessionDataTask!
    
    func executeRequest (commd:String, path:String, timeout:Int)
    {
        if(httpTask != nil){
            httpTask.cancel()
            httpSession.invalidateAndCancel()
            httpSession = nil
        }
        let url:URL = URL(string: path)!
        let configuration = URLSessionConfiguration.default
        if(httpSession == nil){
            httpSession = URLSession(configuration: configuration,
                                     delegate: self, delegateQueue:nil)
        }
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        request.timeoutInterval = TimeInterval(timeout)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        httpTask = httpSession.dataTask(with: (request as URLRequest))
        {
            (dataa, response, error) in
            guard let _:Data = dataa, let _:URLResponse = response, error == nil
                
                else
            {
                if(error != nil){
                    print("error:\(error!.localizedDescription)")
                    if let error = error as NSError? {
                        print("error code:\(error.code)")
                        //if(error.code != -999){//request cancelled
                            self.delegate?.httpRequestError(commd:commd)
                        //}
                    }else{
                        self.delegate?.httpRequestError(commd:commd)
                    }
                }
                
                return
            }
            
            do{
                let dic:[String:AnyObject] = try JSONSerialization.jsonObject(with: dataa!, options: []) as! [String:AnyObject]
                //print("vc 得到資料 dic:  \(dic)")
                
                self.delegate?.httpResponseResult(commd:commd, returnDic:dic)
                
            }
            catch let error as NSError{
                print("MutableDictonary1 error: \(error)")
                self.delegate?.httpRequestError(commd:commd)
                
            }
        }
        httpTask.resume()
 
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        print ("URLSession didReceive")
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(.useCredential, credential)
            
        }

    }
}
