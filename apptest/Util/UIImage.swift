//
//  UIImage.swift
//
//  Created by Joseph on 2019/2/18.
//  Copyright © 2019 Joseph. All rights reserved.
//
import UIKit

extension UIImageView {
    
    func startRotate() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = .pi * 2.0
        rotationAnimation.duration = 1.0
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = MAXFLOAT
        layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func stopRotate() {
        layer.removeAllAnimations()
    }
}

