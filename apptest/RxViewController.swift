//
//  RxViewController.swift
//  apptest
//
//  Created by Joseph on 2019/3/11.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Moya
import Alamofire

class RxViewController: UIViewController {

    @IBOutlet weak var mTitleBar: UIView!
    @IBOutlet weak var mTable: UITableView!
    @IBOutlet weak var mSearchBar: UISearchBar!
    
    var fullScreenSize = UIScreen.main.bounds.size
    
    var path = "https://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000225-002"
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    let viewModel = ViewModel()
    var header: CustomRefreshHeader!
    
    let refreshControl = UIRefreshControl()
    var refreshView: RefreshView!
    
    //var parkingList:Observable<[Parking]>?
    var parkings = PublishSubject<[Parking]>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupiPhoneXLayout()
        
        let nib = UINib(nibName: "DetailDataCell", bundle: nil)
        mTable.register(nib, forCellReuseIdentifier: "DetailDataCell")
        mTable.isScrollEnabled = true
        mTable.rowHeight = UITableView.automaticDimension
        mTable.estimatedRowHeight = 300
        mTable.separatorStyle = .singleLine
        
        // 分隔線的間距 四個數值分別代表 上、左、下、右 的間距
        if(fullScreenSize.width > 414){
            mTable.separatorInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
        }else{
            mTable.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        
        mTable.allowsSelection = true
        // 是否可以多選 cell
        mTable.allowsMultipleSelection = false
        mTable.tableFooterView = UIView()
        /*header = CustomRefreshHeader(refreshingBlock: {
            self.viewModel.refreshParkingList()
        })
        header.arrowView.isHidden = true
        mTable.mj_header = header*/
        mTable.refreshControl = refreshControl
        
        mTable.rx.setDelegate(self).disposed(by: disposeBag)
        
        mSearchBar.returnKeyType = .done
        
        refreshControl.addTarget(self, action:
            #selector(RxViewController.refreshParkingList) ,
                                 for: .valueChanged)
        
        self.setupBindings()
        
        viewModel.requestParkingList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ViewController viewWillAppear")
        
    }
    
    @objc func refreshParkingList(){
        viewModel.refreshParkingList()
    }
    
    func setupBindings(){
        viewModel.loading
            .bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        
        viewModel.refreshing
            .bind(to: self.refreshControl.rx.isRefreshing).disposed(by: disposeBag)
        
        viewModel
            .parkings
            .observeOn(MainScheduler.instance)
            .bind(to: self.parkings)
            .disposed(by: disposeBag)
        
        parkings.bind(to: mTable.rx.items) { (tableView, rhhow, element) in
            print("mTable bind")
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailDataCell") as! DetailDataCell
            cell.accessoryType = .disclosureIndicator
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.name?.text = element.name
            cell.area?.text = element.area
            cell.service_time?.text = element.servicetime
            cell.address?.text = element.address
            return cell
            }.disposed(by: disposeBag)
        
        mTable.rx
            .modelSelected(Parking.self)
            .subscribe(onNext: { (parking) in
                self.gotoSearchFilePage(parking:parking)
            })
            .disposed(by: disposeBag)
        
        //改变刷新状态
        /*viewModel.refreshStatus.asObservable().subscribe(onNext: { (status) in
            self.refreshStatus(status: status,tableView: self.mTable)
        }).disposed(by: disposeBag)*/
        
        mSearchBar.rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: {query in
                //self.parkings = self.parkings.filter { $0.hasPrefix(query) }
                self.viewModel.searchKeywords(keyword:query)
            })
            .disposed(by: disposeBag)
        
        mSearchBar.rx.cancelButtonClicked
            .subscribe(onNext: { _ in
                print("cancelButtonClicked")
                self.mSearchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
        mSearchBar.rx.searchButtonClicked
            .subscribe(onNext: { _ in
                print("searchButtonClicked")
                self.mSearchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
        mSearchBar.rx.textDidEndEditing
            .subscribe(onNext: { _ in
            print("textDidEndEditing")
                self.mSearchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
    }
    
    /**
     设置刷新状态
     */
    func refreshStatus(status:RefreshStatus,tableView: UITableView) {
        switch status {
        case .InvalidData: // 无效的数据
            tableView.mj_header.endRefreshing()
            //tableView.mj_footer.endRefreshing()
            return
        case .DropDownSuccess: // 下拉成功
            tableView.mj_header.endRefreshing()
            //tableView.mj_footer.resetNoMoreData()
        case .PullSuccessHasMoreData: // 上拉，还有更多数据
           // tableView.mj_footer.endRefreshing()
            break
        case .PullSuccessNoMoreData: // 上拉，没有更多数据
           //tableView.mj_footer.endRefreshingWithNoMoreData()
            break
        }
        tableView.mj_header.endRefreshing()
    }
    
    /*func getDataRequest(){
        print("getDataRequest")
        /*viewModel.requestParkingList {(parking) in
            print("parking:\(parking)")
        }*/
        parkingList = viewModel.requestParkingList()
        
        parkingList?.bind(to: mTable.rx.items) { (tableView, rhhow, element) in
            print("mTable bind")
            DispatchQueue.main.async(execute: {
                self.finishActivityIndicator()
            })
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailDataCell") as! DetailDataCell
            cell.accessoryType = .disclosureIndicator
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.name?.text = element.name
            cell.area?.text = element.area
            cell.service_time?.text = element.servicetime
            cell.address?.text = element.address
            return cell
            }.disposed(by: disposeBag)
        
        mTable.rx
            .modelSelected(Parking.self)
            .subscribe(onNext: { (parking) in
                self.gotoSearchFilePage(parking:parking)
            })
            .disposed(by: disposeBag)
        
    }*/
    
    func gotoSearchFilePage(parking:Parking) {
        let detailVC: DetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        detailVC.detailData = parking
        present(detailVC, animated: true, completion: nil)
    }
    
    /*func handleResponse(data:Data){
        DispatchQueue.main.async(execute: {
            self.finishActivityIndicator()
        })
        let decoder = JSONDecoder()
        do{
            let parkingRequest = try decoder.decode(ParkingRequest.self, from: data)
            
            print("success:\(parkingRequest.success)")
            print("parking:\(parkingRequest.result.records)")
        }catch{
            print("error:\(error)")
        }
        
    }*/
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupiPhoneXLayout(){
        if(fullScreenSize.width == 375.0 && fullScreenSize.height == 812.0){//for iPhone X
            print("Constraints layout for iPhone X")
            self.mTitleBar.translatesAutoresizingMaskIntoConstraints = false
            
            //mTitleBar
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 90))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
            
        }else if(fullScreenSize.width == 414.0 && fullScreenSize.height == 896.0){//for iPhone xs max and xr
            print("Constraints layout for iPhone xs max && xr")
            self.mTitleBar.translatesAutoresizingMaskIntoConstraints = false
            
            //mTitleBar
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
            
        }
    }
    
}

extension RxViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    //To prevent jumping you should save heights of cells when they loads and give exact value in tableView:estimatedHeightForRowAt IndexPath
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("willDisplay")
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 300.0
    }
    
}
