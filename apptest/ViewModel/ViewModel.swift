//
//  ViewModel.swift
//  apptest
//
//  Created by Joseph on 2019/3/12.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Realm
import RealmSwift

class ViewModel: BaseViewModel {
    
    public let parkings : PublishSubject<[Parking]> = PublishSubject()
    
    func requestParkingList(completed: @escaping (_ parking: [Parking]) -> ()){
        provider.rx
            .request(.getParkingList())
            .map(ParkingRequest.self)
            .subscribe(onSuccess: { parkingRequest in
                print("success:\(parkingRequest.success)")
                print("parking:\(parkingRequest.result.records)")
                completed(parkingRequest.result.records)
                let container = try! Container()
                try! container.write { transaction in
                    transaction.beginWrite()
                    for record in parkingRequest.result.records{
                        transaction.add(record, update: true)
                    }
                    transaction.commitWrite()
                }
            },onError: { error in
                print("数据请求失败!错误原因：", error)
                
            }).disposed(by: disposeBag)
    }
    
    /*func requestParkingList() -> Observable<[Parking]>{
        /*return provider.rx
            .request(.getParkingList())
            .map(ParkingRequest.self)
            .asObservable()*/
        return provider.rx
            .request(.getParkingList())
            .map(ParkingRequest.self)
            .map{ $0.result.records }
            .asObservable()
    }*/
    
    func requestParkingList() {
        self.loading.onNext(true)
        provider.rx
            .request(.getParkingList())
            .map(ParkingRequest.self)
            .subscribe(onSuccess: { parkingRequest in
                print("success:\(parkingRequest.success)")
                print("parking:\(parkingRequest.result.records)")
                self.loading.onNext(false)
                self.parkings.onNext(parkingRequest.result.records)
                let container = try! Container()
                try! container.write { transaction in
                    transaction.beginWrite()
                    for record in parkingRequest.result.records{
                        transaction.add(record, update: true)
                    }
                    transaction.commitWrite()
                }
            },onError: { error in
                print("数据请求失败!错误原因：", error)
                self.loading.onNext(false)
            }).disposed(by: disposeBag)
    }
    
    func refreshParkingList() {
        provider.rx
            .request(.getParkingList())
            .map(ParkingRequest.self)
            .subscribe(onSuccess: { parkingRequest in
                print("success:\(parkingRequest.success)")
                print("parking:\(parkingRequest.result.records)")
                self.parkings.onNext(parkingRequest.result.records)
                let container = try! Container()
                try! container.write { transaction in
                    transaction.beginWrite()
                    for record in parkingRequest.result.records{
                        transaction.add(record, update: true)
                    }
                    transaction.commitWrite()
                }
                self.refreshStatus.value = .DropDownSuccess
                self.refreshing.onNext(false)
            },onError: { error in
                print("数据请求失败!错误原因：", error)
                self.refreshStatus.value = .InvalidData
                self.refreshing.onNext(false)
            }).disposed(by: disposeBag)
    }
    
    func searchKeywords(keyword:String){
        var RemArrs:[Parking]
        let realm = try! Realm()
        if(keyword != ""){
            let predicate = NSPredicate(format: "area CONTAINS %@ OR address CONTAINS %@",keyword,keyword)
            RemArrs = realm.objects(Parking.self).filter(predicate).toArray(ofType: Parking.self) as [Parking]
        }else{
            RemArrs = realm.objects(Parking.self).toArray(ofType: Parking.self) as [Parking]
        }
        self.parkings.onNext(RemArrs)
        print("RemArrs:\(RemArrs)")
    }
    
}
