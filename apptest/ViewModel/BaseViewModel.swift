//
//  BaseViewModel.swift
//  apptest
//
//  Created by Joseph on 2019/3/12.
//  Copyright © 2019 Joseph. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Moya

enum RefreshStatus: Int {
    case InvalidData // 无效的数据
    case DropDownSuccess // 下拉成功
    case PullSuccessHasMoreData // 上拉，还有更多数据
    case PullSuccessNoMoreData // 上拉，没有更多数据
}

class BaseViewModel: NSObject {
    let disposeBag = DisposeBag()
    let provider = MoyaProvider<RequestAPI>()
    var refreshStatus = Variable(RefreshStatus.InvalidData)
    var loading: PublishSubject<Bool> = PublishSubject()
    var refreshing: PublishSubject<Bool> = PublishSubject()
}
