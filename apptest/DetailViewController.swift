//
//  DetailViewController.swift
//  apptest
//
//  Created by Joseph on 2019/2/19.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mTitleBar: UIView!
    @IBOutlet weak var mTitle: UILabel!
    @IBOutlet weak var mBack: CustomBackBtn!
    @IBOutlet weak var mTable: UITableView!
    @IBOutlet weak var mMap: MKMapView!
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    var fullScreenSize = UIScreen.main.bounds.size
    
    //var detailData:[String:AnyObject] = [String:AnyObject]()
    var detailData:Parking?
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    var destinationLocation:CLLocationCoordinate2D?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupiPhoneXLayout()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.requestLocationAccess()
        
        mTable.delegate = self
        mTable.dataSource = self
        mTable.isScrollEnabled = false
        mTable.rowHeight = UITableView.automaticDimension
        mTable.estimatedRowHeight = 300
        mTable.separatorStyle = .singleLine
        
        // 分隔線的間距 四個數值分別代表 上、左、下、右 的間距
        if(fullScreenSize.width > 414){
            mTable.separatorInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
        }else{
            mTable.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        
        mTable.allowsSelection = false
        // 是否可以多選 cell
        mTable.allowsMultipleSelection = false
        mTable.tableFooterView = UIView()
        
        self.showLocationOnMap()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func doBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupiPhoneXLayout(){
        if(fullScreenSize.width == 375.0 && fullScreenSize.height == 812.0){//for iPhone X
            print("Constraints layout for iPhone X")
            self.mTitleBar.translatesAutoresizingMaskIntoConstraints = false
            
            //mTitleBar
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 90))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
            
        }else if(fullScreenSize.width == 414.0 && fullScreenSize.height == 896.0){//for iPhone xs max and xr
            print("Constraints layout for iPhone xs max && xr")
            self.mTitleBar.translatesAutoresizingMaskIntoConstraints = false
            
            //mTitleBar
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
            
        }
    }
    
    //MARK: tableView delegate
    // 必須實作的方法：每一組有幾個 cell
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 4
        
    }
    
    // 必須實作的方法：每個 cell 要顯示的內容
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            
            let index = indexPath.row
            
            let nib = UINib(nibName: "CustomTableCell", bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: "CustomTableCell")
            let cell: CustomTableCell = tableView.dequeueReusableCell(withIdentifier: "CustomTableCell") as! CustomTableCell
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            
            // 重新加载单元格数据
            print("list row:\(indexPath.row)")
            switch index {
            case 0:
                cell.title?.text = "停車場名稱"
                cell.subtitle?.text = detailData?.name//detailData["name"] as? String
                break
            case 1:
                cell.title?.text = "區域"
                cell.subtitle?.text = detailData?.area//detailData["area"] as? String
                break
            case 2:
                cell.title?.text = "營業時間"
                cell.subtitle?.text = detailData?.servicetime//detailData["service_time"] as? String
                break
            case 3:
                cell.title?.text = "地址"
                cell.subtitle?.text = detailData?.address//detailData["address"] as? String
                break
            default:
                break
            }
            
            return cell
    }
    
    
    
    // 點選 cell 後執行的動作
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        // 取消 cell 的選取狀態
        tableView.deselectRow(
            at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //return CGFloat.leastNormalMagnitude
        return 0
    }
    
    //To prevent jumping you should save heights of cells when they loads and give exact value in tableView:estimatedHeightForRowAt IndexPath
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("willDisplay")
        cellHeights[indexPath] = cell.frame.size.height
        
        
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 300.0
    }
    
    @IBAction func doGotoParking(_ sender: Any) {
        self.showMenu()
    }
    
    func showLocationOnMap(){
        //getCoordinate(detailData["address"] as! String) { (location) in
        getCoordinate(detailData?.address ?? "") { (location) in
            guard let location = location else { return }
            self.destinationLocation = location
            let xScale:CLLocationDegrees = 0.001
            let yScale:CLLocationDegrees = 0.001
            let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: xScale, longitudeDelta: yScale)
            let region:MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
            self.mMap.setRegion(region, animated: false)
            self.mMap.isZoomEnabled = true
            
            // 設定地圖標記，標題為公司名稱，副標題為公司地址
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = "\(self.detailData?.name)"//"\(self.detailData["name"] as! String)"
            annotation.subtitle = "\(self.detailData?.address)"//"\(self.detailData["address"] as! String)"
            self.mMap.addAnnotation(annotation)
            
        }
    }
    
    // 透過地址取得地圖的座標
    func getCoordinate(_ address:String, completion: @escaping (CLLocationCoordinate2D?) -> ()) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard error == nil else {
                print("error: \(error)")
                completion(nil)
                return
            }
            completion(placemarks?.first?.location?.coordinate)
        }
    }
    
    func showDirection(){
        self.mMap.delegate = self
        
        if(self.currentLocation != nil){
            let sourceLocation = self.currentLocation!.coordinate
            let destinationLocation = self.destinationLocation!
            
            let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
            let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
            
            let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
            let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
            
            let directionRequest = MKDirections.Request()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem
            directionRequest.transportType = .automobile
            
            let directions = MKDirections(request: directionRequest)
            
            directions.calculate {
                (response, error) -> Void in
                
                guard let response = response else {
                    if let error = error {
                        print("Error: \(error)")
                    }
                    
                    return
                }
                
                let route = response.routes[0]
                self.mMap.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
                
                let rect = route.polyline.boundingMapRect
                self.mMap.setRegion(MKCoordinateRegion(rect), animated: true)
            }
        }else{
            self.showTryAgainLater()
        }
        
    }
    
    func gotoMapApplication(){
        if(self.currentLocation != nil){
            let sourceLocation = self.currentLocation!.coordinate
            let destinationLocation = self.destinationLocation!
            let PA = MKMapItem(placemark:MKPlacemark(coordinate:sourceLocation, addressDictionary:nil))
            let PB = MKMapItem(placemark:MKPlacemark(coordinate:destinationLocation, addressDictionary:nil))
            PA.name = "你的位置"
            PB.name = detailData?.name//detailData["name"] as? String
            let points = [PA, PB]
            let options = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
            MKMapItem.openMaps(with: points, launchOptions: options)
        }else{
            self.showTryAgainLater()
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    // MARK - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations[0] }
        print("didUpdateLocations \(currentLocation?.coordinate.latitude)")
    }
    
    func requestLocationAccess() {
        locationManager.requestWhenInUseAuthorization()
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("location authorizedAlways")
            self.mMap.showsUserLocation = true
            self.locationManager.startUpdatingLocation()
            return
            
        case .denied, .restricted:
            print("location access denied")
            
        default:
            break
        }
    }
    
    //MARK: - CLLocationManager, Support for location update
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            print("authorizedWhenInUse");
            self.mMap.showsUserLocation = true
            self.locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            print("authorizedWhenInUse");
            self.mMap.showsUserLocation = true
            self.locationManager.startUpdatingLocation()
            break
        case .denied:
            print("denied");
            
            break
        case .notDetermined:
            break
        case .restricted:
            print("restricted");
            break
        }
        
    }
    
    func showMenu(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let photo = UIAlertAction(title: "路線規劃", style: .default, handler: { _ in
            self.showDirection()
        })
        let browse = UIAlertAction(title: "地圖導航", style: .default, handler: { _ in
            self.gotoMapApplication()
        })
        //action.setValue(image?.withRenderingMode(.alwaysOriginal), forKey: "image")
        //photo.setValue(UIImage(named: "ic_photo.png"), forKey: "image")
        //browse.setValue(UIImage(named: "ic_photo_more.png"), forKey: "image")
        actionSheet.addAction(photo)
        actionSheet.addAction(browse)
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: .cancel, handler: nil))
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            actionSheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            actionSheet.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showTryAgainLater(){
        let alertController = UIAlertController(title: "無法取得位置資訊", message: "請稍候再試", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "好", style: UIAlertAction.Style.default) {
            UIAlertAction in
            print("Ok Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
