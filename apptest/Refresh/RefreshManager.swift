//
//  RefreshManager.swift
//  apptest
//
//  Created by Joseph on 2019/3/13.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit
import MJRefresh

//MARK; - 自定义下拉刷新控件
class CustomRefreshHeader: MJRefreshNormalHeader {
    
    //var loadingImage: UIImageView?
    //var refreshView:RefreshView!
    
    // 处理不同刷新状态下的组件状态
    override var state: MJRefreshState {
        didSet {
            switch state {
            case .idle:
                print("MJRefreshState idle")
                //self.refreshView.mLoading.stopRotate()
                //self.refreshView.isHidden = true
                UIApplication.shared.endIgnoringInteractionEvents()
            case .pulling:
                print("MJRefreshState pulling")
                //self.refreshView.isHidden = false
                //self.refreshView.mLoading.startRotate()
                UIApplication.shared.beginIgnoringInteractionEvents()
            case .refreshing:
                print("MJRefreshState refreshing")
                //self.refreshView.isHidden = false
                //self.refreshView.mLoading.startRotate()
                //UIApplication.shared.beginIgnoringInteractionEvents()
            default:
                print("")
            }
        }
    }
    
    // 初始化组件
    override func prepare() {
        super.prepare()
        self.mj_h = 50
        
        //self.loadingImage = UIImageView(image: UIImage(named: "ic_loading.png"))
        //self.loadingImage?.backgroundColor = UIColor.white
        //self.refreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView
        //self.refreshView.frame = self.frame
        //self.addSubview(loadingImage!)
        //self.addSubview(self.refreshView)
        
        self.lastUpdatedTimeLabel.isHidden = true
        
        // Hide the status
        self.stateLabel.isHidden = true
        
        self.arrowView.isHidden = true
        
    }
    
    // 组件定位
    override func placeSubviews() {
        super.placeSubviews()
        //self.bringSubviewToFront(self.refreshView)
    }
}
