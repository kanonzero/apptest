//
//  ViewController.swift
//  apptest
//
//  Created by Joseph on 2019/2/18.
//  Copyright © 2019 Joseph. All rights reserved.
//

import UIKit

class ViewController: UIViewController, HttpManagerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var mTitleBar: UIView!
    @IBOutlet weak var mTable: UITableView!
    @IBOutlet weak var mSearchBar: UISearchBar!
    
    var fullScreenSize = UIScreen.main.bounds.size
    
    var mCustomActivityIndicator: CustomActivityIndicator!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var mHttpManager:HttpManager = HttpManager()
    
    let refreshControl = UIRefreshControl()
    
    var parkingListArray:Array<[String:AnyObject]> = Array()
    var parkingNameArray:Array<String> = Array()
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    var refreshView: RefreshView!
    
    var isLaunch = true
    
    let command = "parking_data"
    
    var path = "https://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000225-002"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupiPhoneXLayout()
        
        mTable.delegate = self
        mTable.dataSource = self
        mTable.isScrollEnabled = true
        mTable.rowHeight = UITableView.automaticDimension
        mTable.estimatedRowHeight = 300
        mTable.separatorStyle = .singleLine
        
        // 分隔線的間距 四個數值分別代表 上、左、下、右 的間距
        if(fullScreenSize.width > 414){
            mTable.separatorInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
        }else{
            mTable.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        
        mTable.allowsSelection = true
        // 是否可以多選 cell
        mTable.allowsMultipleSelection = false
        mTable.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action:
            #selector(ViewController.refreshParkingListData) ,
                                 for: .valueChanged)
        self.mTable.refreshControl = refreshControl
        self.getRefereshView()
        
        // search bar
        self.mSearchBar.placeholder = "搜尋行政區或停車場名稱關鍵字"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ViewController viewWillAppear")
        
        self.mSearchBar.delegate = self
        self.mHttpManager.delegate = self
        if(self.isLaunch == true){
            self.startActivityIndicator()
            self.getDataRequest()
        }
        self.isLaunch = false
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func refreshParkingListData(){
        DispatchQueue.main.async(execute: {
            self.refreshView.mLoading.startRotate()
            UIApplication.shared.beginIgnoringInteractionEvents()
        })
        mSearchBar.text = ""
        self.getDataRequest()
    }
    
    func getDataRequest(){
        self.mHttpManager.executeRequest (commd: command, path:path, timeout:30)
    }
    
    func refreshParkingList(){
        self.parkingListArray = self.appDelegate.db!.selectParkingData()
        self.mTable.reloadData()
        self.mTable.isHidden = false
    }
    
    func httpResponseResult(commd: String, returnDic: [String : AnyObject]) {
        if(commd == self.command){
            //print("returnDic:\(returnDic)")
            let success = returnDic["success"] as! Int
            if(success == 1){
                do{
                    self.appDelegate.db?.deleteParkingData()
                    let resultlist = returnDic["result"] as! [String:AnyObject]
                    let recordsDic = resultlist["records"] as! Array<[String:AnyObject]>
                    if(recordsDic.count > 0){
                        for i in 0..<recordsDic.count
                        {
                            let refreshData = recordsDic as [Any]
                            self.appDelegate.db?.insertParkingDataAll(refreshData[i] as! [String:AnyObject])
                        }
                        DispatchQueue.main.async(execute: {
                            self.finishActivityIndicator()
                            if(self.refreshView != nil){
                                self.refreshView.mLoading.stopRotate()
                            }
                            self.refreshControl.endRefreshing()
                            self.refreshParkingList()
                        })
                    }
                }catch let error as NSError{
                    DispatchQueue.main.async(execute: {
                        self.finishActivityIndicator()
                    })
                }
            }else{
                DispatchQueue.main.async(execute: {
                    self.finishActivityIndicator()
                })
            }
         }
    }
    
    func httpRequestError(commd: String) {
        if(commd == self.command){
            DispatchQueue.main.async(execute: {
                self.finishActivityIndicator()
                self.showErrorMsg()
            })
        }
    }
    
    //MARK: tableView delegate
    // 必須實作的方法：每一組有幾個 cell
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return parkingListArray.count
        
    }
    
    // 必須實作的方法：每個 cell 要顯示的內容
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            
            let nib = UINib(nibName: "DetailDataCell", bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: "DetailDataCell")
            let cell: DetailDataCell = tableView.dequeueReusableCell(withIdentifier: "DetailDataCell") as! DetailDataCell
            cell.accessoryType = .disclosureIndicator
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            
            // 重新加载单元格数据
            print("list row:\(indexPath.row)")
            
            if(parkingListArray.count > 0){
                cell.name?.text = parkingListArray[indexPath.row]["name"] as? String
                cell.area?.text = parkingListArray[indexPath.row]["area"] as? String
                cell.service_time?.text = parkingListArray[indexPath.row]["service_time"] as? String
                cell.address?.text = parkingListArray[indexPath.row]["address"] as? String
            }
            return cell
    }
    
    
    
    // 點選 cell 後執行的動作
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        // 取消 cell 的選取狀態
        tableView.deselectRow(
            at: indexPath, animated: true)
        
        let index = indexPath.row
        
        self.gotoSearchFilePage(dic:self.parkingListArray[index])
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //return CGFloat.leastNormalMagnitude
        return 0
    }
    
    //To prevent jumping you should save heights of cells when they loads and give exact value in tableView:estimatedHeightForRowAt IndexPath
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("willDisplay")
        cellHeights[indexPath] = cell.frame.size.height
        
        
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 300.0
    }
    
    // MARK: searchBar delegate
    
    func searchBarTextDidBeginEditing(_: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.refreshKeywordList()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        mSearchBar.resignFirstResponder()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Do Search Action")
        mSearchBar.resignFirstResponder()
        self.refreshKeywordList()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancel presss")
        mSearchBar.text = ""
    }
    
    func scrollViewWillBeginDragging(_: UIScrollView) {
        mSearchBar.endEditing(true)
    }
    
    // MARK: SearchSettingView delegate
    func updateSearchBarText(current:String){
        print("updateSearchBarText:\(current)")
        self.mSearchBar.text = current
    }
    
    func doSearchAction(keyword: String) {
        print("doSearchAction keyword:\(keyword)")
        self.parkingListArray = self.appDelegate.db!.selectParkingData(keyword:keyword)
        self.mTable.reloadData()
    }
    
    func refreshKeywordList(){
        let textCount = mSearchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
        let textIsNotEmpty = textCount > 0
        
        if textIsNotEmpty {
            let keyword: String = mSearchBar.text!
            print("keyword:\(keyword)")
            doSearchAction(keyword: keyword)
            
        } else {
            print("empty keyword")
            self.refreshParkingList()
        }
    }
    
    func gotoSearchFilePage(dic:[String:AnyObject]) {
        let detailVC: DetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        //detailVC.detailData = dic
        present(detailVC, animated: true, completion: nil)
    }
    
    func getRefereshView() {
        if let objOfRefreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView {
            refreshView = objOfRefreshView
            refreshView.frame = refreshControl.frame
            refreshControl.addSubview(refreshView)
        }
    }
    
    func startActivityIndicator(){
        mCustomActivityIndicator = CustomActivityIndicator(frame: CGRect.zero)
        self.view.addSubview(mCustomActivityIndicator)
        
        mCustomActivityIndicator.getCustomActivityIndicator().startRotate()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func finishActivityIndicator(){
        if(mCustomActivityIndicator != nil){
            mCustomActivityIndicator.getCustomActivityIndicator().stopRotate()
            mCustomActivityIndicator.removeFromSuperview()
            mCustomActivityIndicator = nil
        }
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    
    func setupiPhoneXLayout(){
        if(fullScreenSize.width == 375.0 && fullScreenSize.height == 812.0){//for iPhone X
            print("Constraints layout for iPhone X")
            self.mTitleBar.translatesAutoresizingMaskIntoConstraints = false
            
            //mTitleBar
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 90))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
            
        }else if(fullScreenSize.width == 414.0 && fullScreenSize.height == 896.0){//for iPhone xs max and xr
            print("Constraints layout for iPhone xs max && xr")
            self.mTitleBar.translatesAutoresizingMaskIntoConstraints = false
            
            //mTitleBar
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
            self.view.addConstraint(NSLayoutConstraint(item: mTitleBar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
            
        }
    }
    
    func showErrorMsg(){
        let alertController = UIAlertController(title: "無法取得停車場資訊", message: "請稍候再試", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "好", style: UIAlertAction.Style.default) {
            UIAlertAction in
            print("Ok Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }



}

